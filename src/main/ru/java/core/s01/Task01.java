package main.ru.java.core.s01;

/**
 * 1. Составить программу, которая печатает заданное слово, начиная
 * с последней буквы.
 * <p>
 * Для выполнения этой задачи мне нужно создать строку
 * и пройтись по этой строке с конца. Для того чтобы пройтись по строке
 * нам нужно использовать цикл.
 */

public class Task01 {
    public static void main(String[] args) {
        String b = "влад";
        System.out.println(reverseWord(b));
    }

    public static String reverseWord(String word) {
        StringBuilder reverseStr = new StringBuilder();
        for (int i = word.length() - 1; i >= 0; i--) {
            reverseStr.append(word.charAt(i));

        }
        return reverseStr.toString();

    }
}

