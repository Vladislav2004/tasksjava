package main.ru.java.core.s01;

/**
 * 2. Дано слово. Добавить к нему в начале конце столько звездочек, сколько
 * букв в этом слове.
 * <p>
 * Для выполнения этой задачи нам нужно создать переменную
 * типа String, создать переменную хранящую символ "*". Нам нужно с помощью цикла
 * узнать длинну слова чтобы кол-во "*" совпадало с кол-во символов в слове.
 * А потом нам нужно "звёздочки + слово + звёздочки".
 */

public class Task02 {
    public static void main(String[] args) {
        String word = "Vlad"; // Строковая переменная
        System.out.println(symbol(word));// вывод переменной из метода "symbol"

    }


    public static String symbol(String word) {
        StringBuilder wordWithStars = new StringBuilder();
        String star = "*";
        for (int i = 0; i < word.length() - 1; i++) {
            wordWithStars.append(star);
            star = star + "*"; // с каждой итерацией цикла прибаляем к переменной star наш символ
        }
        return (star + word + star);//звездочки + перменная + звёздочки


    }
}

