package main.ru.java.core.s01;

/**
 * 3. Дан текст и символ. Определить количество данного символа в тексте.
 * <p>
 * Для решения этой задачи нам нужно создать две переменные.
 * Первая для слова в котором будет искаться символ.
 * Вторая для символа.
 * Так-же нам нужно создать счётчик для считывания кол-во символа.
 * Нам нужно создать цикл для того чтобы пройтись по слову и если там будет
 * тот символ который мы искали то символ нужно прибавить к счётчику
 */

public class Task03 {
    public static void main(String[] args) {
        String word = "мамамамам";
        char symbol = 'м';

        System.out.println(SearchOneSymbol(word, symbol));


    }


    public static int SearchOneSymbol(String text, char c) {
        int b = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == c) {
                b++;
            }
        }
        return b;

    }
}

