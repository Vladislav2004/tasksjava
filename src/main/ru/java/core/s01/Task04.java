package main.ru.java.core.s01;

/**
 * 4. Дан текст. В нем слова разделены одним пробелом
 * (начальные и конечные пробелы и символ "-" в тексте отсутствуют).
 * Определить количество слов в тексте.
 * <p>
 * <p>
 * Для выполения этой задачи нам нужно пройтись по нашему тексту, если нам в тексте
 * попадется символ " " то нам нужно прибавить к счётчику 1.
 * <p>
 * // испоьзовать indexOf
 */
//подсчёт слов без пробелов

public class Task04 {
    public static void main(String[] args) {
        String text1 = "Я когда нибудь изучу java5";
        System.out.println(quantityOfSpace(text1));

    }

    public static int quantityOfSpace(String text) {
        StringBuilder resultString = new StringBuilder();
        String[] countBuilder;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                resultString.append("\n");
            } else {
                resultString.append(text.charAt(i));
            }
        }
        countBuilder = resultString.toString().split("\n");
        return countBuilder.length;
    }


}
