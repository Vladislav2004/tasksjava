package main.ru.java.core.s01;

/**
 * 5. Дано слово. Проверить, является ли оно "перевертышем"
 * (перевертышем называется слово, читаемое одинаково как с начала, так и с конца).
 * <p>
 * <p>
 * Для выполения этой задачи нам нужно создать две переменные типа String.
 * Нам нужно их сравнить и если они будут равны друг другу то это слово будет являться
 * перевёртышем. Так-же с помощью цикла мы должны пройтись по ним с обратной стороны.
 */
public class Task05 {
    public static void main(String[] args) {
        String word = "иди";
        System.out.println(checkPalindrome(word));

    }


    public static String checkPalindrome(String word) {
        StringBuilder makeStr = new StringBuilder();
        for (int i = word.length() - 1; i >= 0; i--) {
            char a = word.charAt(i);
            makeStr.append(a); //возвращает обновленный объект
        }
        if ((makeStr.toString()).equals(word)) {
            return ("Это слово является перевертышем");

        } else {
            return ("Это слово не является перевертышем");
        }


    }


}
